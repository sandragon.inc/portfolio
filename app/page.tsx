'use client'
import Image from 'next/image'
import { useEffect } from 'react'

export default function Home() {

  useEffect( () => {
    (
      async () => {
        const LocomotiveScroll = (await import('locomotive-scroll')).default;
        const locomotiveScroll = new LocomotiveScroll
      }
    )()
  }, [])

  return (
    <>
      <div>
        <h1>Hello World!</h1>
      </div>
    </>
  )
}
