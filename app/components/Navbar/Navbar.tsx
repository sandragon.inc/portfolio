import Link from 'next/link'

import style from './Navbar.module.css'

export function Navbar() {
    return (
        <>
            <div className={style.navbar_container}>
                <div className={style.logo}>Logo</div>
                <div>
                    <ul className={style.options}>
                        <li><Link href={"./"}>About</Link></li>
                        <li><Link href={"./"}>Projects</Link></li>
                        <li><Link href={"./"}>Contact</Link></li>
                    </ul>
                </div>
            </div>
        </>
    )
}