'use client'

import style from './Hero.module.css'
import Image from 'next/image'
import gsap from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'
import { useLayoutEffect, useRef } from 'react'

export function Hero() {

    const backgroundImage = useRef(null)
    const heroImage = useRef(null)
    useLayoutEffect( () => {
        gsap.registerPlugin(ScrollTrigger)

        const timeline = gsap.timeline({
            scrollTrigger: {
                trigger: document.documentElement,
                start: "top",
                end: "+=500px",
                scrub: true,
                markers: true
            }
        })

        timeline
            // .from(heroImage.current, { clipPath: "inset(15%)" })
            .to(heroImage.current, { height: "200px" }, 0)
    }, [])

    return (
        <>
            <div className={style.hero}>
                <div ref={backgroundImage} className={style.hero_background}>
                    <Image
                        src={'/images/hero_bg.jpg'}
                        fill={true}
                        alt='Hero Image'>
                    </Image>
                </div>
                <div className={style.hero_container}>
                    <div ref={heroImage} data-scroll data-scroll-speed="0.3" className={style.hero_image}>
                        <Image
                            src={'/images/hero.jpg'}
                            fill={true}
                            alt='Background Image'>
                        </Image>
                    </div>
                    <h1 data-scroll data-scroll-speed="0.7">Fukuri</h1>
                </div>
            </div>
        </>
    )
}