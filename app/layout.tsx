import './globals.css'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import { Navbar } from './components/Navbar/Navbar'
import { Hero } from './components/Hero/Hero'
import style from './page.module.css'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Portfolio',
  description: 'Team Portfolio',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <Navbar />
        <main className={style.main}>
          <Hero />
          <div className={style.div_sample}></div>
          <div className={style.div_sample}></div>
          <div className={style.div_sample}></div>
        </main>
          {children}
        </body>
    </html>
  )
}
